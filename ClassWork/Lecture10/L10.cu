#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "png_util.h"

__global__ void mandelbrot(const int NRe, 
		const int NIm, 
		const float minRe,
		const float minIm,
		const float dRe, 
		const float dIm,
		float * h_count){

  int tx = threadIdx.x;
  int bx = blockIdx.x;
  int dx = blockDim.x;

  int ty = threadIdx.y;
  int by = blockIdx.y;
  int dy = blockDim.y;

  int imag = tx + bx*dx;
  int real = ty + by*dy;

  if(real < NRe && imag < NIm){

    int n,m;
  
    for(m=0;m<NIm;++m){
      for(n=0;n<NRe;++n){
      
        float cRe = minRe + n*dRe;
        float cIm = minIm + m*dIm;

        float zRe = 0;
        float zIm = 0;
      
        int Nt = 200;
        int t, cnt=0;
        for(t=0;t<Nt;++t){
	
	  // z = z^2 + c
	  //   = (zRe + i*zIm)*(zRe + i*zIm) + (cRe + i*cIm)
	  //   = zRe^2 - zIm^2 + 2*i*zIm*zRe + cRe + i*cIm
	  float zReTmp = zRe*zRe - zIm*zIm + cRe;
	  zIm = 2.f*zIm*zRe + cIm;
	  zRe = zReTmp;

	  cnt += (zRe*zRe+zIm*zIm<4.f);
        }

        h_count[n + m*NRe] = cnt;
      }
    }
  }
}


int main(int argc, char **argv){

  cudaSetDevice(4);

  const int NRe = 4096;
  const int NIm = 4096;

  /* box containing sample points */
  const float centRe = -.759856, centIm= .125547;
  const float diam  = 0.151579;
  const float minRe = centRe-0.5*diam;
  const float remax = centRe+0.5*diam;
  const float minIm = centIm-0.5*diam;
  const float immax = centIm+0.5*diam;

  const float dRe = (remax-minRe)/(NRe-1.f);
  const float dIm = (immax-minIm)/(NIm-1.f);

  float *h_count = (float*) calloc(NRe*NIm, sizeof(float));

  // DEVICE VERSION COUNT
  float *c_count;

  // allocate an array on the DEVICE
  cudaMalloc(&c_count, NRe*NIm*sizeof(float));

  // timing
  cudaEvent_t start, end;
  
  cudaEventCreate(&start);
  cudaEventCreate(&end);

  // copy data from HOST to DEVICE
  cudaMemcpy(c_count, h_count, NRe*NIm*sizeof(float), cudaMemcpyHostToDevice);

  // call mandelbrot from here
  dim3 B(16,16,1);
  dim3 G ((NRe+16-1)/16, (NIm+16-1)/16, 1);

  // starting the time recording
  cudaDeviceSynchronize();
  cudaEventRecord(start);
  
  mandelbrot <<< G , B >>> (NRe, NIm, minRe, minIm, dRe, dIm, h_count);

  // ending the time recording
  cudaEventRecord(end);
  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, start, end);
  elapsed /= 1000;

  // copy data from DEVICE to HOST
  cudaMemcpy(h_count, c_count, NRe*NIm*sizeof(float), cudaMemcpyDeviceToHost);
  
  printf("elapsed time %g\n", elapsed);

  FILE *png = fopen("mandelbrot.png", "w");
  write_hot_png(png, NRe, NIm, h_count, 0, 80);
  fclose(png);

  cudaFree(c_count);
  free(h_count);

  return 0;
}
