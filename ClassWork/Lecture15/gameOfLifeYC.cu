#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>

extern "C"
{
#include "png_util.h"
}

#define NX 16
#define NY 16

// to compile
// make

// to run
// make run

// to convert output png files to an mp4 movie:
// make movie

/* function to convert from (i,j) cell index to linear storage index */
__device__ __host__ int idx(int N, int i, int j){
  int n = i + (N + 2)*j;
  return n;  
}
/*
// function to update Inew from Iold 
void iterate(int N, float *Iold, float *Inew){

  for(int i=1;i<N+1;++i){ // notice the loop bounds are [1,N]
    for(int j=1;j<N+1;++j){ // notice the loop bounds are [1,n]

      // sum up 8 neighbor values (total number of alive neighbors)
      int n = Iold[idx(N,i-1,j-1)] + Iold[idx(N,i+1,j-1)] + Iold[idx(N,i,j-1)]
	+ Iold[idx(N,i-1,j)]   + Iold[idx(N,i+1,j)]   + Iold[idx(N,i,j+1)]
	+ Iold[idx(N,i-1,j+1)] + Iold[idx(N,i+1,j+1)];
      
      // distilled version
      int oldState = Iold[idx(N,i,j)];
      int newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3) ;
      Inew[idx(N,i,j)] = newState;
    }
  }
}
*/

/* function to update Inew from Iold via Kernel */
__global__ void iterateKernel(int N, float *Iold, float *Inew){

  __shared__ float s_old[NX][NY];
  __shared__ float s_new[NX][NY];

  int tx = threadIdx.x;
  int bx = blockIdx.x;
  int dx = blockDim.x;

  int ty = threadIdx.y;
  int by = blockIdx.y;
  int dy = blockDim.y;

  int col = tx + bx*dx;
  int row = ty + by*dy;

  int Ni = N;
  int Nj = N;

  if(row < Ni+1 && col < Nj+1 && 0<row && 0<col){

   
    int n = Iold[idx(N,row-1,col-1)] + Iold[idx(N,row+1,col-1)] + Iold[idx(N,row,col-1)]
      + Iold[idx(N,row-1,col)]   + Iold[idx(N,row+1,col)]   + Iold[idx(N,row,col+1)]
      + Iold[idx(N,row-1,col+1)] + Iold[idx(N,row+1,col+1)];
      
    // distilled version
    int oldState = Iold[idx(N,row,col)];
    int newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3);
    Inew[idx(N,row,col)] = newState;

    // read from DEVICE memory
    float Arco = Iold[n];
    float Arcn = Inew[n];

    // collaborate write to the shared memory on-SM
    s_old[tx][ty] = Arco;
    s_new[tx][ty] = Arcn;
  }

  __syncthreads();
}

#define P 1054

// kernel that loads in parallel and uses a binary tree reduction
__global__ void compareKernelV4(int N, float *x, float *y, float *ans){

  volatile __shared__ float s_aMinusb[P];
  
  int t = threadIdx.x;
  int b = blockIdx.x;
  int n = t + b*P;

  float tmp = 0;
  while(n<N){
    tmp += fabs(y[n]-x[n]);
    n += gridDim.x*blockDim.x;
  }
  s_aMinusb[t] = tmp;

  __syncthreads();

  // only thread 0 is doing work in each thread-block
  int alive=P/2;
  while(alive>=1){
    
    if(t<alive)
      s_aMinusb[t] += s_aMinusb[t+alive];

    if(alive>32)
      __syncthreads();
    
    alive /= 2;
  }
  
  if(t==0){
    ans[b] = s_aMinusb[0];
  }
}

/* function to print game board for debugging */
void print_board(int N, float *board){
  printf("\n");
  for(int i=1; i<N+1; i=i+1){
    for(int j=1; j<N+1; j=j+1){
      printf("%d", (int)board[idx(N,i,j)]);
    }
    printf("\n");
  }
  printf("\n");
}

# if 0  

//  function to solve for game board using Game of Life rules 
void solve(int N){

  // time_t t;
  /* Intializes integer random number generator */
  //  srand((unsigned) time(&t));
  srand(123456);

  // notice the size of these arrays
  float* Inew = (float*) calloc((N+2)*(N+2),sizeof(float));
  float* Iold = (float*) calloc((N+2)*(N+2),sizeof(float));
  float* h_ans = (float*) calloc((N+2)*(N+2), sizeof(float));

  for(int i=1;i<N+1;i=i+1){
    for(int j=1;j<N+1;j=j+1){
      // set board state randomly to 1 or 0 
      Iold[idx(N,i,j)] = rand()%2;
    }
  }

  float *Inew_c, *Iold_c, *c_ans;
  cudaMalloc(&c_a, N*sizeof(float));
  cudaMalloc(&c_b, N*sizeof(float));
  cudaMalloc(&c_ans, N*sizeof(float));
  cudaMemcpy(c_a, h_a, N*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_b, h_b, N*sizeof(float), cudaMemcpyHostToDevice);
  
  /* print initial board*/
  printf("initial game board:");
//  print_board(N, Iold);

  /* iterate here */
  int count = 0;   // step counter
  int iostep = 1; // output every iostep
  int output = 1;  // save images if output=1
  int maxsteps = 1000; // maximum number of steps
  do{
    /* iterate from Iold to Inew */
    iterate(N, Iold, Inew);
    
    /* iterate from Inew to Iold */
    iterate(N, Inew, Iold);

    if(output==1 && count%iostep==0){
//      char filename[BUFSIZ];
//      FILE *png;
//      sprintf(filename, "gol%05d.png", count/iostep);
//      png = fopen(filename, "w");
//      write_gray_png(png, N+2, N+2, Iold, 0, 1);
//      fclose(png);
    }
    
    /* update counter */
    count = count + 1;
  }while(runKernel(N, c_a, c_b, c_ans, h_ans)!=0 && count <= maxsteps);
  
  /* print out the cell existence in the whole board, then in cell (1 1) and (10 10)*/
  printf("final game board:");
  print_board(N, Iold);
  printf("I_{1 1} = %d\n",   (int)Iold[idx(N,1,1)]);
  printf("I_{10 10} = %d\n", (int)Iold[idx(N,10,10)]);
  printf("Took %d steps\n", count);
  free(Inew);
  free(Iold);
}

float runKernel(int N, float *c_a, float *c_b, float *c_ans, float *h_ans){

  float res = 0;
  dim3 T(NX,NY,1);
  dim3 A((N+NX-1)/NX, (N+NY-1)/NY);
  compareKernelV4 <<< T, A >>> (N, c_a, c_b, c_ans);
  cudaMemcpy(h_ans, c_ans, (B.x+2)*(B.x+2)*sizeof(float), cudaMemcpyDeviceToHost);
  for(int n=0;n<G.x;++n){
    res += h_ans[n];
  }
  }
  return res;
}

# endif

/* function to solve the game board using Game of Life rules with the Kernel */
void solveKernel(int N){

  // set up events
  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);
  
  /* Intializes integer random number generator */
  //  srand((unsigned) time(&t));
  srand(123456);

  // notice the size of these arrays
//  float* Inew = (float*) calloc((N+2)*(N+2),sizeof(float));
  float* Iold = (float*) calloc((N+2)*(N+2),sizeof(float));
  float* Inew = (float*) calloc((N+2)*(N+2),sizeof(float));
  float* h_ans = (float*) calloc((N+2)*(N+2), sizeof(float));

  for(int i=1;i<N+1;i=i+1){
    for(int j=1;j<N+1;j=j+1){
      // set board state randomly to 1 or 0 
      Iold[idx(N,i,j)] = rand()%2;
    }
  }
  
  /* print initial board*/
  printf("initial game board:");
  print_board(N, Iold);

  float *Inew_c, *Iold_c, *c_ans;
  cudaMalloc(&Inew_c, (N+2)*(N+2)*sizeof(float));
  cudaMalloc(&Iold_c, (N+2)*(N+2)*sizeof(float));
  cudaMalloc(&c_ans, (N+2)*(N+2)*sizeof(float));
  cudaMemcpy(Inew_c, Inew, (N+2)*(N+2)*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(Iold_c, Iold, (N+2)*(N+2)*sizeof(float), cudaMemcpyHostToDevice);

  dim3 B(NX,NY,1);
  dim3 G((N+NX-1)/NX, (N+NY-1)/NY,1);

  cudaEventRecord(tic);

  /* iterate here */
  int count = 0;   // step counter
  int iostep = 1; // output every iostep
  int output = 1;  // save images if output=1
  int maxsteps = 1000; // maximum number of steps
  do{
    /* iterate from Iold to Inew */
    iterateKernel <<< G , B >>> (N, Iold_c, Inew_c);
    
    /* iterate from Inew to Iold */
    iterateKernel <<< G , B >>> (N, Inew_c, Iold_c);

    cudaMemcpy(Iold, Iold_c, (N+2)*(N+2)*sizeof(float), cudaMemcpyDeviceToHost);
    cudaMemcpy(Inew, Inew_c, (N+2)*(N+2)*sizeof(float), cudaMemcpyDeviceToHost);
   
    if(output==1 && count%iostep==0){
//      char filename[BUFSIZ];
//      FILE *png;
//      sprintf(filename, "gol%05d.png", count/iostep);
//      png = fopen(filename, "w");
//      write_gray_png(png, N+2, N+2, Iold, 0, 1);
//      fclose(png);
    }
    
    /* update counter */
    count = count + 1;
  }while(runKernel(N, Iold_c, Inew_c, c_ans, h_ans)!=0 && count <= maxsteps);
  
  /* print out the cell existence in the whole board, then in cell (1 1) and (10 10)*/
  
  cudaEventRecord(toc);
  cudaMemcpy(Iold, Iold_c, (N+2)*(N+2)*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(Inew, Inew_c, (N+2)*(N+2)*sizeof(float), cudaMemcpyDeviceToHost);

  float elapsed;
  cudaEventSynchronize(toc); 
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000;
 // float time_spent = elapsed;
  printf("Time spent = %e sec\n", elapsed);

  
  printf("final game board:");
  print_board(N, Iold);
  printf("I_{1 1} = %d\n",   (int)Iold[idx(N,1,1)]);
  printf("I_{10 10} = %d\n", (int)Iold[idx(N,10,10)]);
  printf("Took %d steps\n", count);
  free(Inew);
  free(Iold);
  cudaFree(Inew_c);
  cudaFree(Iold_c);
}

/* usage: ./main 100 
         to iterate, solve and display the game board of size N*/
int main(int argc, char **argv){
  /* read N from the command line arguments */
  int N = atoi(argv[1]);

  /* to solve for cell existence in game of life game board */
  solveKernel(N);

  return 0;
}