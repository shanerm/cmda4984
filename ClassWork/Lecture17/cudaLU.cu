#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <omp.h>

// original LU code:
// https://en.wikipedia.org/wiki/LU_decomposition

// sets type of matrices
#define dfloat double

// hostLU:
// a. compute the LU decompositions of N matrices of size MxM
// b. assumes each matrix stored in column major

void hostLU(int N, int M, const dfloat *A, dfloat *LU, dfloat tol){

  // loop over matrices
  for(int n=0;n<N;++n){

    int i, j, k;

    // pointer to nth A and LU matrix storage
    const dfloat *An = A+n*M*M;
    dfloat *LUn = LU+n*M*M;

    // i will be column
    // j will be row
    
    // note we assume column major storage
    for (j = 0; j < M; ++j) {
      for (i = 0; i < M; ++i) {
	LUn[j + M*i] = An[j + M*i];
      }
    }

    // loop over columns
    for (i = 0; i < M; ++i) {
      // loop over rows starting from diagonal
      for (j = i + 1; j < M; ++j) {

	if(fabs(LUn[i*M+i])<tol){
	  printf("hostLU: matrix diagonal %g indicates LU breakdown, exiting\n",
		 LUn[i*M+i]);
	}
	
	LUn[j+M*i] /= LUn[i*M+i];
	
	for (k = i + 1; k < M; k++)
	  LUn[j+M*k] -= LUn[j+M*i] * LUn[i+M*k];
      }
    }
  }
}

__global__ void deviceLUv0(int N, int M, const dfloat *A, dfloat *LU, dfloat tol){

  // loop over matrices
  //  for(int n=0;n<N;++n)
  {
    int n = blockIdx.x;
    // assuming blockDim.x == 1

    int i, j, k;

    // pointer to nth A and LU matrix storage
    const dfloat *An = A+n*M*M;
    dfloat *LUn = LU+n*M*M;

    // i will be column
    // j will be row
    
    // note we assume column major storage
    for (j = 0; j < M; ++j) {
      for (i = 0; i < M; ++i) {
	LUn[j + M*i] = An[j + M*i];
      }
    }

    // loop over columns
    for (i = 0; i < M; ++i) {
      // loop over rows starting from diagonal
      for (j = i + 1; j < M; ++j) {

	if(fabs(LUn[i*M+i])<tol){
	  printf("hostLU: matrix diagonal %g indicates LU breakdown, exiting\n",
		 LUn[i*M+i]);
	}
	
	LUn[j+M*i] /= LUn[i*M+i];
	
	for (k = i + 1; k < M; k++)
	  LUn[j+M*k] -= LUn[j+M*i] * LUn[i+M*k];
      }
    }
  }
}

// M threads per thread-block, each thread responsible for one row
__global__ void deviceLUv1(int N, int M, const dfloat *A, dfloat *LU, dfloat tol){

  // loop over matrices
  //  for(int n=0;n<N;++n)
  {
    int n = blockIdx.x;
    int j = threadIdx.x;

    int i, k;

    // pointer to nth A and LU matrix storage
    const dfloat *An = A+n*M*M;
    dfloat *LUn = LU+n*M*M;

    // i will be column
    // j will be row
    
    // note we assume column major storage (for coalescing)
    for (i = 0; i < M; ++i) {
      LUn[j + M*i] = An[j + M*i];
    }

    // loop over columns
    for (i = 0; i < M; ++i) {
      
      __syncthreads();
      
      // loop over rows starting from diagonal
      //      for (j = i + 1; j < M; ++j)
      if(j>=i+1){
	LUn[j+M*i] /= LUn[i*M+i];
	
	for (k = i + 1; k < M; k++)
	  LUn[j+M*k] -= LUn[j+M*i] * LUn[i+M*k];
      }
    }
  }
}


// M threads per thread-block, each thread responsible for one row
// use shared memory
template < int sM >
__global__ void deviceLUv2(int N, int M, const dfloat *A, dfloat *LU, dfloat tol){

  // loop over matrices
  //  for(int n=0;n<N;++n)
  {
    int n = blockIdx.x;
    int j = threadIdx.x;

    int i, k;

    __shared__ dfloat s_LUn[sM][sM];
    
    // pointer to nth A and LU matrix storage
    const dfloat *An = A+n*M*M;
    dfloat *LUn = LU+n*M*M;

    // i will be column
    // j will be row
    
    // note we assume column major storage (for coalescing)
    for (i = 0; i < M; ++i) {
      s_LUn[j][i] = An[j + M*i];
    }

    // loop over columns
    for (i = 0; i < M; ++i) {
      
      __syncthreads();
      
      // loop over rows starting from diagonal
      //      for (j = i + 1; j < M; ++j)
      if(j>=i+1){
	s_LUn[j][i] /= s_LUn[i][i];
	
	for (k = i + 1; k < M; k++)
	  s_LUn[j][k] -= s_LUn[j][i] * s_LUn[i][k];
      }

      LUn[j+M*i] = s_LUn[j][i];
    }
  }
}

// THIS IS THE KERNEL TO USE FOR LECTURE ASSIGNMENT 17
template < int sM >
__global__ void deviceLU(int N, int M, const dfloat *A, dfloat *LU, dfloat tol){

  // loop over matrices
  //  for(int n=0;n<N;++n)
  {
    int n = blockIdx.x;
    int j = threadIdx.x;

    int i, k;

    __shared__ dfloat s_LUn[sM][sM];
    
    // pointer to nth A and LU matrix storage
    const dfloat *An = A+n*M*M;
    dfloat *LUn = LU+n*M*M;

    // i will be column
    // j will be row
    
    // note we assume column major storage (for coalescing)
    for (i = 0; i < M; ++i) {
      s_LUn[j][i] = An[j + M*i];
    }

    // loop over columns
#pragma unroll
    for (i = 0; i < M; ++i) {
      
      __syncthreads();
      
      // loop over rows starting from diagonal
      //      for (j = i + 1; j < M; ++j)

      dfloat tmp = s_LUn[j][i];

      if(j>=i+1){
	tmp /= s_LUn[i][i];
	
	for (k = i + 1; k < M; k++)
	  s_LUn[j][k] -= tmp * s_LUn[i][k];
      }
      
      LUn[j+M*i] = tmp;
    }
  }
}
// My code with the huge throughput (no idea how)
template < int sM >
__global__ void devicePLU(int N, int M, const dfloat *A, dfloat *LU, int *P, dfloat tol){

  // loop over matrices
  //  for(int n=0;n<N;++n)
  {
    int n = blockIdx.x;
    int j = threadIdx.x;

    int i, k;

    dfloat tmp;

    __shared__ dfloat s_LUn[sM][sM];

    // pointer to nth A and LU matrix storage
    const dfloat *An = A+n*M*M;
    dfloat *LUn = LU+n*M*M;
    int *Pn = P + n*M;

    // initialize pivot
    for(int j=0; j<M; ++j){
      Pn[j] = j;
    }

    // i will be column
    // j will be row

    // note we assume column major storage (for coalescing)
    for (i = 0; i < M; ++i) {
      s_LUn[j][i] = An[j + M*i];
    }

    // loop over columns
#pragma unroll
    for (i = 0; i < M; ++i) {

      __syncthreads();

      // find pivot row
      dfloat maxLU = 0.0, absLUji = 0.0;
      int imax = i;
      int tmpi = 0;
      
      // find entry in column i with largest abs value
      #define mask 0xffffffff
      #define warp 32

      tmp = __shfl_down_sync(mask, tmp, 16, warp);
      tmpi = __shfl_down_sync(mask, imax, 16, warp);
      if(fabs(tmp) > maxLU){
        maxLU = tmp;
	imax = tmpi;
      }
      
      tmp = __shfl_down_sync(mask, tmp,  8, warp);
      tmpi = __shfl_down_sync(mask, imax, 8, warp);
      if(fabs(tmp) > maxLU){
	maxLU =	fabs(tmp);
        imax = tmpi;
      }

      tmp = __shfl_down_sync(mask, tmp,  4, warp);
      tmpi = __shfl_down_sync(mask, imax, 4, warp);
      if(fabs(tmp) > maxLU){
	maxLU =	fabs(tmp);
        imax = tmpi;
      }

      tmp = __shfl_down_sync(mask, tmp,  2, warp);
      tmpi = __shfl_down_sync(mask, imax, 2, warp);
      if(fabs(tmp) > maxLU){
        maxLU = fabs(tmp);
        imax = tmpi;
      }

      tmp = __shfl_down_sync(mask, tmp,  1, warp);
      tmpi = __shfl_down_sync(mask, imax, 1, warp);
      if(fabs(tmp) > maxLU){
        maxLU = fabs(tmp);
        imax = tmpi;
      }


      if (imax != i) {
        //pivoting P
	if (j==0){
           int temp = Pn[i];
	   Pn[i] = Pn[imax];
           Pn[imax] = temp;
	}

	//pivoting rows of A
        // swap rows i and imax
        dfloat tmp1 = LUn[i+M*j];
        LUn[i + M*j] = LUn[imax+M*j];
        LUn[imax+M*j] = tmp1;
      }


      // loop over rows starting from diagonal
      //      for (j = i + 1; j < M; ++j)

      tmp = s_LUn[j][i];

      if(j>=i+1){
        tmp /= s_LUn[i][i];

	for (k = i + 1; k < M; k++)
          s_LUn[j][k] -= tmp * s_LUn[i][k];
      }
    }
    LUn[j+M*i] = tmp;
  }
}



// store matrix n in column major in shared memory (avoids bank conflicts)
template < int sM >
__global__ void deviceLUv4(int N, int M, const dfloat * __restrict__ A, dfloat * __restrict__ LU, const dfloat tol){

  // loop over matrices
  //  for(int n=0;n<N;++n)
  {
    int n = blockIdx.x;
    int j = threadIdx.x;

    int i, k;

    __shared__ dfloat s_LUn[sM][sM];
    
    // pointer to nth A and LU matrix storage
    const dfloat *An = A+n*M*M;
    dfloat *LUn = LU+n*M*M;

    // i will be column
    // j will be row
    
    // note we assume column major storage (for coalescing)
    for (i = 0; i < M; ++i) {
      s_LUn[i][j] = An[j + M*i];
    }

    // loop over columns
#pragma unroll
    for (i = 0; i < M; ++i) {
      
      __syncthreads();
      
      // loop over rows starting from diagonal
      //      for (j = i + 1; j < M; ++j)
      
      dfloat tmp = s_LUn[i][j];
      if(j>=i+1){
	tmp /= s_LUn[i][i];
	
	for (k = i + 1; k < M; k++)
	  s_LUn[k][j] -= tmp * s_LUn[k][i];
      }
      LUn[j+M*i] = tmp;
    }
  }
}


template <int tM>
void runKernelLU(int op, int N, int M, const dfloat *c_a, dfloat *c_LU, int *c_P, dfloat tol){
  switch(op){
  case 0: deviceLUv0    <<<N,1>>> (N,M,c_a, c_LU,tol); break;
  case 1: deviceLUv1    <<<N,M>>> (N,M,c_a, c_LU,tol); break;
  case 2: deviceLUv2<tM><<<N,M>>> (N,M,c_a, c_LU,tol); break;
  case 3: deviceLU  <tM><<<N,M>>> (N,M,c_a, c_LU,tol); break; // version to use in lec 17 assignment
  case 4: deviceLUv4<tM><<<N,M>>> (N,M,c_a, c_LU,tol); break;
  case 5: devicePLU <tM><<<N,M>>> (N,M,c_a, c_LU, c_P, tol); break;
  }
}

void runKernel(int op, int N, int M, const dfloat *c_A, dfloat *c_LU, int *c_P, dfloat tol){
  switch(M){
  case  1: runKernelLU< 1>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case  2: runKernelLU< 2>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case  3: runKernelLU< 3>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case  4: runKernelLU< 4>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case  5: runKernelLU< 5>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case  6: runKernelLU< 6>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case  7: runKernelLU< 7>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case  8: runKernelLU< 8>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case  9: runKernelLU< 9>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 10: runKernelLU<10>(op, N, M, c_A, c_LU, c_P, tol); break;
  case 11: runKernelLU<11>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 12: runKernelLU<12>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 13: runKernelLU<13>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 14: runKernelLU<14>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 15: runKernelLU<15>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 16: runKernelLU<16>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 17: runKernelLU<17>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 18: runKernelLU<18>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 19: runKernelLU<19>(op, N, M, c_A, c_LU, c_P, tol); break;
  case 20: runKernelLU<20>(op, N, M, c_A, c_LU, c_P, tol); break;
  case 21: runKernelLU<21>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 22: runKernelLU<22>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 23: runKernelLU<23>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 24: runKernelLU<24>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 25: runKernelLU<25>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 26: runKernelLU<26>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 27: runKernelLU<27>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 28: runKernelLU<28>(op, N, M, c_A, c_LU, c_P, tol); break;	
  case 29: runKernelLU<29>(op, N, M, c_A, c_LU, c_P, tol); break;
  case 30: runKernelLU<30>(op, N, M, c_A, c_LU, c_P, tol); break;
  case 31: runKernelLU<31>(op, N, M, c_A, c_LU, c_P, tol); break;
  case 32: runKernelLU<32>(op, N, M, c_A, c_LU, c_P, tol); break;	
  }
}


double runDeviceLU(int op, int N, int M, const dfloat *c_A, dfloat *c_LU, int *c_P, dfloat tol){

  cudaDeviceSynchronize();

  cudaEvent_t tic, toc;
  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  // launched (possibly templated kernel)
  runKernel(op, N, M, c_A, c_LU, c_P, tol);
  cudaEventRecord(toc);

  cudaDeviceSynchronize();
  
  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.;
  return elapsed;
}


// pivoted version of hostPLU
void hostPLU(int N, int M, const dfloat *A, dfloat *LU, int *P, dfloat tol){

  // loop over matrices
  for(int n=0;n<N;++n){

    int i, j, k;

    // pointer to nth A and LU matrix storage
    const dfloat *An  = A +n*M*M;
    dfloat *LUn = LU+n*M*M;
    int    *Pn  = P +n*M; 

    // initialize pivot 
    for(int j=0;j<M;++j){
      Pn[j] = j;
    }    
    
    // i will be column
    // j will be row
    
    // note we assume column major storage
    for (j = 0; j < M; ++j) {
      for (i = 0; i < M; ++i) {
	LUn[j + M*i] = An[j + M*i];
      }
    }

    // loop over columns
    for (i = 0; i < M; ++i) {
      
      // find pivot row
      dfloat maxLU = 0.0, absLUji = 0.0;
      int imax = i;
      
      for (j = i; j < M; j++){
	// find entry in column i with largest abs value
	absLUji = fabs(LUn[j+i*M]);
	if (absLUji > maxLU) { 
	  maxLU = absLUji;
	  imax = j;
	}
      }
      
      if (maxLU < tol){ printf("hostPivotedLU broke down\n"); exit(-1);} //failure, matrix is degenerate
      
      if (imax != i) {
	//pivoting P
	j = Pn[i];
	Pn[i] = Pn[imax];
	Pn[imax] = j;

	//pivoting rows of A
	for(j=0;j<M;++j){
	  // swap rows i and imax
	  dfloat tmp = LUn[i+M*j];
	  LUn[i + M*j] = LUn[imax+M*j];
	  LUn[imax+M*j] = tmp;
	}
      }

      // now do LU decomposition on row permuted matrix
      // loop over rows starting from diagonal
      for (j = i + 1; j < M; ++j) {

	if(fabs(LUn[i*M+i])<tol){
	  printf("hostLU: matrix diagonal %g indicates LU breakdown, exiting\n",
		 LUn[i*M+i]);
	}
	
	LUn[j+M*i] /= LUn[i*M+i];
	
	for (k = i + 1; k < M; k++)
	  LUn[j+M*k] -= LUn[j+M*i] * LUn[i+M*k];
      }
    }
  }
}



int main(int argc, char **argv){

  if(argc<3) {
    printf("usage: ./cudaLU totalMatrixEntries matrixWidth\n");
    exit(-1);
  }
  
  int Ntotal = atoi(argv[1]);
  int M = atoi(argv[2]);

  int N = (Ntotal+M*M-1)/(M*M);
  
  dfloat *h_A  = (dfloat*) calloc(N*M*M, sizeof(dfloat));
  dfloat *h_LU = (dfloat*) calloc(N*M*M, sizeof(dfloat));
  int    *h_P  = (int*)    calloc(N*M,   sizeof(int));

  for(int n=0;n<N;++n){
    dfloat *An = h_A + n*M*M;
    for(int j=0;j<M;++j){ // row
      for(int i=0;i<M;++i){ // column
	An[j+i*M] = drand48();
      }
      // make diagonally dominant (so that pivoting is not necessary)
      An[j*M+j] += M;
    }
  }

  dfloat tol = 1e-14;

  // 1. test hostLU
  double ticLU = omp_get_wtime();
  hostLU(N, M, h_A, h_LU, tol);  
  double tocLU = omp_get_wtime();
  double elapsedLU = tocLU-ticLU;
  dfloat bandwidthLU = (2.*N*M*M/(elapsedLU*1.e9))*sizeof(dfloat); // matrix bandwidth in GB/s (ignoring caching)
  
  printf("hostLU,    M:%02d, time:%3.2e s, diff:%3.2e, throughput:%3.2f GB/s\n",
	 M, elapsedLU, 0., bandwidthLU);

  // 1.0 v0 kernel run
  dfloat elapsedTimes[1000];
  dfloat *c_A, *c_LU;
  int *c_P;
  dfloat *h_gpuLU = (dfloat*) calloc(N*M*M, sizeof(dfloat));
  cudaMalloc(&c_A, N*M*M*sizeof(dfloat));
  cudaMalloc(&c_LU, N*M*M*sizeof(dfloat));
  cudaMalloc(&c_P, N*M*M*sizeof(int));

  cudaMemcpy(c_A, h_A, N*M*M*sizeof(dfloat), cudaMemcpyHostToDevice);

  // warm up
  int Nops = 6;
  for(int op=0;op<Nops;++op){
    runDeviceLU(op, N, M, c_A, c_LU, c_P, tol);

    // zero c_LU on DEVICE
    cudaMemset(c_LU, 0, N*M*M*sizeof(dfloat));
    
    elapsedTimes[op] = runDeviceLU(op, N, M, c_A, c_LU, c_P, tol);
    
    cudaMemcpy(h_gpuLU, c_LU, N*M*M*sizeof(dfloat), cudaMemcpyDeviceToHost);
    cudaMemcpy(h_P, c_P, N*M*M*sizeof(int), cudaMemcpyDeviceToHost);
    
    dfloat maxDiff =0;
    for(int n=0;n<N*M*M;++n){
      dfloat diff = fabs(h_gpuLU[n]-h_LU[n]);
      maxDiff = (maxDiff>diff) ? maxDiff:diff;
    }

    // matrix bandwidth in GB/s (ignoring caching)
    dfloat bandwidthLUop = (2.*N*M*M/(elapsedTimes[op]*1.e9))*sizeof(dfloat); 
    
    printf("kernel:%02d, M:%02d, time:%3.2e s, diff:%3.2e, throughput:%3.2f GB/s\n",
	   op, M, elapsedTimes[op], maxDiff, bandwidthLUop);
  }
  
#if 0
  // 2. test hostPLU (pivoted LU)
  double ticPLU = omp_get_wtime();
  hostPLU(N, M, h_A, h_LU, h_P, tol);
  double tocPLU = omp_get_wtime();
  double elapsedPLU = tocPLU-ticPLU;
  dfloat bandwidthPLU = (2.*N*M*M/(elapsedPLU*1.e9))*sizeof(dfloat); // matrix bandwidth in GB/s (ignoring caching)
  
  printf("hostPLU took %g to factorize %d matrices of size %d x %d with throughput %g GB/s\n",
	 elapsedPLU, N, M, M, bandwidthPLU);
#endif
  
  return 0;
}
