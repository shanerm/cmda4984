// https://stackoverflow.com/questions/52831987/how-to-use-wmma-functions
#include <stdlib.h>
#include <stdio.h>
#include <cuda.h>
#include <cuda_fp16.h>
#include <mma.h>
#include <iostream>

/*
  1. THIS CODE CAN ONLY BE RUN ON VOLTA AND LATER GENERATION GPUS
  2. YOU CAN USE a V100 GPU ON THE ARC cascades CLUSTER !
  nvcc -arch=sm_70 -o cudaTensorUnitsIntro cudaTensorUnitsIntro.cu
 */

using namespace nvcuda;

#define DIM 16

// V1: batch matrix power operator:  Z <= X^NITS * Y
// evaluate as a sequence of matrix multiplications
// store the intermediate matrix results in global DEVICE memory (c_halfZ)
__global__ void matrixPowerKernelV1(int NITS, int N, half *c_halfX, half *c_halfY, half *c_halfZ){

  int e = blockIdx.x;
  
  // use same A for all thread-blocks, different B and C
  half  *Xe = c_halfX + DIM*DIM*e;
  half  *Ye = c_halfY + DIM*DIM*e;
  half  *Ze = c_halfZ + DIM*DIM*e;

  // Declare the fragments
  wmma::fragment<wmma::matrix_a,    DIM, DIM, DIM, half, wmma::row_major> Ae_frag;
  wmma::fragment<wmma::matrix_b,    DIM, DIM, DIM, half, wmma::col_major> Be_frag;
  wmma::fragment<wmma::accumulator, DIM, DIM, DIM, half> Ce_frag;
  
  // Load the inputs
  wmma::load_matrix_sync(Ae_frag, Xe, DIM);
  wmma::load_matrix_sync(Be_frag, Ye, DIM);

  // Xe => Ae
  // Ye => Be
  // Ce = 0
  // repeatedly evaluate:
  //   Ce_frag = Ae_frag*Be_frag
  //   Ze <= Ce_frag
  //   Be_frag <= Ze
  for(int it=0;it<NITS;++it){

    // zero accumulator
    wmma::fill_fragment(Ce_frag, 0.0f);

    // Perform the matrix multiplication  ( Ce_frag = Ae_frag*Be_frag )
    wmma::mma_sync(Ce_frag, Ae_frag, Be_frag, Ce_frag);

    // Ce_frag => Ze (need to store accumulator to copy it to Be)
    wmma::store_matrix_sync(Ze, Ce_frag, DIM, wmma::mem_row_major);
    
    // Load the Be_frag from Ze
    wmma::load_matrix_sync(Be_frag, Ze, DIM);
  }

  // store the result in Ze
  wmma::store_matrix_sync(Ze, Ce_frag, DIM, wmma::mem_row_major);
}

// V2: batch matrix power operator:  Z <= X^NITS * Y
// evaluate as a sequence of matrix multiplications
// store the intermediate matrix results in shared memory (s_halfZ)
__global__ void matrixPowerKernelV2(int NITS, int N, half *c_halfX, half *c_halfY, half *c_halfZ){

  int e = blockIdx.x;
  
  // use same A for all thread-blocks, different B and C
  half  *Xe = c_halfX + DIM*DIM*e;
  half  *Ye = c_halfY + DIM*DIM*e;
  half  *Ze = c_halfZ + DIM*DIM*e;

  // shared array used to transfer data from accumulator to fragment
  __shared__ half s_Be[DIM*DIM];
    
  // Declare the fragments
  wmma::fragment<wmma::matrix_a,    DIM, DIM, DIM, half, wmma::row_major> Ae_frag;
  wmma::fragment<wmma::matrix_b,    DIM, DIM, DIM, half, wmma::col_major> Be_frag;
  wmma::fragment<wmma::accumulator, DIM, DIM, DIM, half> Ce_frag;

  // Load the inputs
  wmma::load_matrix_sync(Ae_frag, Xe, DIM);
  wmma::load_matrix_sync(Be_frag, Ye, DIM);

  // Xe => Ae_frag
  // Ye => Be_frag
  // Ce = 0
  // repeatedly evaluate:
  //   Ce = Ae_frag*Be_frag
  //   Be_frag <= Ce_frag
  for(int it=0;it<NITS;++it){

    // zero accumulator
    wmma::fill_fragment(Ce_frag, 0.0f);

    // Perform the matrix multiplication  ( Ce = Ae*Be )
    wmma::mma_sync(Ce_frag, Ae_frag, Be_frag, Ce_frag);

    // store the accumulator to shared
    wmma::store_matrix_sync(s_Be, Ce_frag, DIM, wmma::mem_row_major);
    
    // load the fragment from shared
    wmma::load_matrix_sync(Be_frag, s_Be, DIM);
  }
  
  // store the result in Ze
  wmma::store_matrix_sync(Ze, Ce_frag, DIM, wmma::mem_row_major);
}


__global__ void float2HalfKernel(int N, float *a, half *b){
	   
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    b[n] = __float2half(a[n]);
  }
}

#define DIM2 256

void float2Half(int N, float *c_a, half *c_halfa){
  int T = DIM2;
  dim3 B(T);
  dim3 G((N+T-1)/T);
  
  float2HalfKernel<<<G,B>>>(N, c_a, c_halfa);
}

__global__ void half2FloatKernel(int N, half *a, float *b){
	   
  int n = threadIdx.x + blockIdx.x*blockDim.x;

  if(n<N){
    b[n] = __half2float(a[n]);
  }
}



void half2Float(int N, half *c_halfa, float *c_a){
  int T = DIM2;
  dim3 B(T);
  dim3 G((N+T-1)/T);
  
  half2FloatKernel<<<G,B>>>(N, c_halfa, c_a);
}

void runKernel(int op, int NITS, int N, half *c_halfA, half *c_halfB, half *c_halfC){

  if(op==0)
    matrixPowerKernelV1 <<<N,32>>>(NITS, N, c_halfA, c_halfB, c_halfC);
  if(op==1)
    matrixPowerKernelV2 <<<N,32>>>(NITS, N, c_halfA, c_halfB, c_halfC);
}

double timeKernel(int op, int NITS, int N, half *c_halfA, half *c_halfB, half *c_halfC){
  
  // warm up
  runKernel(op, NITS, N, c_halfA, c_halfB, c_halfC);
  cudaDeviceSynchronize();
  
  cudaEvent_t tic, toc;

  cudaEventCreate(&tic);
  cudaEventCreate(&toc);

  cudaEventRecord(tic);
  
  runKernel(op, NITS, N, c_halfA, c_halfB, c_halfC);
    
  cudaEventRecord(toc);
  cudaDeviceSynchronize();

  float elapsed;
  cudaEventElapsedTime(&elapsed, tic, toc);
  elapsed /= 1000.;

  unsigned long long int flops = NITS*N*(unsigned long long int)((DIM*DIM*DIM*2));
  
  double flopsThroughput = (flops/1.e9)/elapsed;
  
  return flopsThroughput;
}

int main(int argc, char **argv){

  if(argc!=3) { printf("usage: ./cudaTensorUnitsIntro N NITS\n"); exit(-1); }

  int N = atoi(argv[1]);
  int NITS = atoi(argv[2]);
  
  float *h_X, *c_X;
  half *c_halfX;

  float *h_Y, *c_Y;
  half *c_halfY;

  float *h_Z, *c_Z;
  half *c_halfZ;

  int NX = DIM*DIM*N;
  int NY = DIM*DIM*N;
  int NZ = DIM*DIM*N;

  h_X = new float[NX];
  h_Y = new float[NY];
  h_Z = new float[NZ];

  // create arrays for X,Y,D
  cudaMalloc(&c_X, NX*sizeof(float));
  cudaMalloc(&c_Y, NY*sizeof(float));
  cudaMalloc(&c_Z, NZ*sizeof(float));

  // build matrices such that 
  for(int n=0;n<N;++n){

    for(int i=0;i<DIM*DIM;++i){
      h_X[n*DIM*DIM + i] = drand48()/sqrt(DIM*DIM);
    }

    for(int i=0;i<DIM*DIM;++i){
      h_Y[n*DIM*DIM + i] = drand48()/sqrt(DIM*DIM);
    }
    for(int i=0;i<DIM*DIM;++i){
      h_Z[n*DIM*DIM + i] = 0;
    }
  }
  
  cudaMemcpy(c_X, h_X, NX*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_Y, h_Y, NY*sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(c_Z, h_Z, NZ*sizeof(float), cudaMemcpyHostToDevice);

  // allocate storage for half precision arrays
  cudaMalloc(&c_halfX, NX*sizeof(half));
  cudaMalloc(&c_halfY, NY*sizeof(half));
  cudaMalloc(&c_halfZ, NZ*sizeof(half));

  // convert float to half
  float2Half(NX, c_X, c_halfX);
  float2Half(NY, c_Y, c_halfY);
  float2Half(NZ, c_Z, c_halfZ);

  // time two variants
  double gflops0 = timeKernel(0, NITS, N, c_halfX, c_halfY, c_halfZ);
  double gflops1 = timeKernel(1, NITS, N, c_halfX, c_halfY, c_halfZ);

  // extract data in float form
  half2Float(NZ, c_halfZ, c_Z);
  cudaMemcpy(h_Z, c_Z, NZ*sizeof(float), cudaMemcpyDeviceToHost);

  printf("V1: throuhgput %g GFLOPS/s\n", gflops0);
  printf("V2: throuhgput %g GFLOPS/s\n", gflops1);
  
#if 0
  for(int e=0;e<10;++e){
    printf("warp %d: \n", e);
    for (int i = 0; i < DIM; i++){
      for(int j = 0; j < DIM; ++j){
	std::cout << h_Z[e*DIM*DIM+ i*DIM + j];
	if(j<15) std::cout << ",";
      }
      std::cout << std::endl;
    }
  }
#endif

}
