#include <stdlib.h>
#include <stdio.h>
#include "cuda.h"

// write a value for x
__global__ void logicalMap(int N, int Nit, float *c_r, float *c_x){

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N){
    float cxn = c_x[n];
    float crn = c_r[n];
    for(int i = 0; i < Nit; ++i)
      cxn = crn*cxn*(1 - cxn);
    c_x[n] = cxn;
  }

}

// HOST main function
int main(int argc, char **argv){

  // set the GPU
  cudaSetDevice(4);

  if(argc<3){ printf("usage: ./cudaLogicalMap N Nit\n"); exit(-1); }

  // read command
  int N = atoi(argv[1]);
  int Nit = atoi(argv[2]);

  // allocate an array on the HOST
  float *h_r = (float*) malloc(N*sizeof(float));
  float *h_x = (float*) malloc(N*sizeof(float));

  // DEVICE VERSION R
  float *c_r;

  // allocate an array on the DEVICE
  cudaMalloc(&c_r, N*sizeof(float));

  // call kernel to set values in array
  for(int l = 0; l < N; ++l)
    h_r[l] = 4.0*((float)l/(float)N);

  // copy data from HOST to DEVICE
  cudaMemcpy(c_r, h_r, N*sizeof(float), cudaMemcpyHostToDevice);

  // DEVICE VERSION X
  float *c_x;

  // allocate an array on the DEVICE
  cudaMalloc(&c_x, N*sizeof(float));

  // call kernel to set values in array
  for(int l = 0; l < N;  ++l)
    h_x[l] = drand48();

  // copy data from HOST to DEVICE
  cudaMemcpy(c_x, h_x, N*sizeof(float), cudaMemcpyHostToDevice);

  // calling the Logical Map
  int B = 256;
  int G = (N+B-1)/B;
  logicalMap <<< G , B >>> (N, Nit, c_r, c_x);

  // copy data from DEVCIE to HOST
  cudaMemcpy(h_r, c_r, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);

  // writing into file
  FILE *fp = fopen("LogicalMap.dat", "w");

  fprintf(fp, "%-10s %10s \n", "r", "x");

  for(int i = 0; i < N; ++i){
    fprintf(fp, "%-10f %10f \n", h_r[i], h_x[i]);
  }

  fclose(fp);

  // free data from DEVICE
  cudaFree(c_x);
  cudaFree(c_r);

  // free data from HOST
  free(h_x);
  free(h_r);

  return 0;
}