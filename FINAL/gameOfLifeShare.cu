#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <cuda.h>
extern "C" {
#include "png_util.h"
}

#define NX 9
#define NY 9


// to compile
// make

// to run
// make run

// to convert output png files to an mp4 movie:
// make movie

__global__ void GameOfLifeShared(int N, float *A, float *A_new)
{
	int tx = threadIdx.x;
        int bx = blockIdx.x;
	int dx = blockDim.x;
  
	int ty = threadIdx.y;
	int by = blockIdx.y;
	int dy = blockDim.y;

	// copy A(iy,ix) to s_matrix(ty,tx). Each block updates (dx-2)*(dy-2) elements
        // (iy, ix) is the global index
	int iy = (dy -2) * by + ty;  
        int ix = (dx -2) * bx + tx;
	int index = iy * (N+2) + ix;
      
       	// Declare the shared memory on each block 
        __shared__ float s_matrix[NY][NX];
	// Copy elements into shared memory
        if (ix < N+2 && iy < N+2)
         	s_matrix[ty][tx] = A[index];

       //Sync all threads in block
        __syncthreads();

       // update
       // make sure we do not update the boundary elements, i.e. the number of updated elements is N*N	
       if (iy < N+1 && ix < N+1) {
	       // Each block only updates interior elements, so we need to throw away the boundary element
           if(ty>0 && ty<dy && tx>0 && tx<dx) {
               // compute the number of neighbourhoods
            int  n = s_matrix[ty+1][tx] + s_matrix[ty-1][tx] 
                            + s_matrix[ty][tx+1] + s_matrix[ty][tx-1] 
                            + s_matrix[ty+1][tx+1] + s_matrix[ty-1][tx-1] 
                            + s_matrix[ty-1][tx+1] + s_matrix[ty+1][tx-1];
 
	 // from shared memory to global memory
     	 int oldState = s_matrix[ty][tx];
	 int newState = (oldState==1) ? ( (n==2)||(n==3) ) : (n==3) ;
         A_new[index] = newState;

         }
       }
} 


/* function to convert from (i,j) cell index to linear storage index */
int idx(int N, int i, int j){
  int n = i + (N + 2)*j;
  return n;  
}

/* function to print game board for debugging */
void print_board(int N, float *board){
  printf("\n");
  for(int i=1; i<N+1; i=i+1){
    for(int j=1; j<N+1; j=j+1){
      printf("%d", (int)board[idx(N,i,j)]);
    }
    printf("\n");
  }
  printf("\n");
}

/* function to solve for game board using Game of Life rules */
void solve(int N){

  cudaSetDevice(1);

  /* Intializes integer random number generator */
  //  srand((unsigned) time(&t));
  srand(123456);

  // notice the size of these arrays
  float* hA_new = (float*) calloc((N+2)*(N+2),sizeof(float));
  float* hA = (float*) calloc((N+2)*(N+2),sizeof(float));

  for(int i=1;i<N+1;i=i+1){
    for(int j=1;j<N+1;j=j+1){
      // set board state randomly to 1 or 0 
      hA[idx(N,i,j)] = rand()%2;
    }
  }
  /* print initial board*/
  printf("initial game board:");
  print_board(N, hA);

  // allocate an array on the DEVICE
  float *cA; float *cA_new;  
  cudaMalloc(&cA, (N+2)*(N+2)*sizeof(float));
  cudaMalloc(&cA_new, (N+2)*(N+2)*sizeof(float));

  // copy data from host to Device
  cudaMemcpy(cA, hA, (N+2)*(N+2)*sizeof(float), cudaMemcpyHostToDevice);

  dim3 B(NX, NY,1);
  dim3 G((N+NX-2-1)/(NX-2), (N+NY-2-1)/(NY-2), 1);

  // create events
  cudaEvent_t start, end;
  cudaEventCreate(&start);
  cudaEventCreate(&end);

  cudaEventRecord(start);

  /* iterate here */
  int count = 0;  // step counter
  int iostep = 1; // output every iostep
  int output = 1;  // save images if output=1
  int maxsteps = 200; // maximum number of steps
  do{
    /* iterate from Iold to Inew */
     GameOfLifeShared<<< G, B >>>(N, cA, cA_new);

    /* iterate from Inew to Iold */
     GameOfLifeShared<<< G, B >>>(N, cA_new, cA);

    cudaMemcpy(hA, cA, (N+2)*(N+2)*sizeof(float), cudaMemcpyDeviceToHost);

    if(output==1 && count%iostep==0){
      char filename[BUFSIZ];
      FILE *png;
      sprintf(filename, "gol%05d.png", count/iostep);
      png = fopen(filename, "w");
      write_gray_png(png, N+2, N+2, hA, 0, 1);
      fclose(png);
    }
    
    /* update counter */
    count = count + 1;
  }while(memcmp(hA_new, hA, (N+2)*(N+2)*sizeof(int))!=0 && count <= maxsteps);
  
  cudaEventRecord(end);
  float elapsed;
  cudaEventSynchronize(end); 
  cudaEventElapsedTime(&elapsed, start, end); 
  elapsed = elapsed/1000.; // convert to seconds

  /* print out the cell existence in the whole board, then in cell (1 1) and (10 10)*/
  printf("final game board:");
  print_board(N, hA);
  printf("I_{1 1} = %d\n",   (int)hA[idx(N,1,1)]);
  printf("I_{10 10} = %d\n", (int)hA[idx(N,10,10)]);
  printf("Took %d steps\n", count);
  printf("Time spent = %g sec\n", elapsed);

  free(hA);
  free(hA_new);
  cudaFree(cA);
  cudaFree(cA_new);
}



/* usage: ./main 100 
         to iterate, solve and display the game board of size N*/
int main(int argc, char **argv){
  /* read N from the command line arguments */
  int N = atoi(argv[1]);

  /* to solve for cell existence in game of life game board */
  solve(N);

//  float time_spent = elapsed;
//  printf("Time spent = %g sec\n", time_spent);
  return 0;
} 