from numba import cuda, float32
import random
from ctypes import CDLL
import numpy as np
import os

NX = 9
NY = 9

@cuda.jit
def GameOfLifeShared(N, A, A_new):
    # Same thing as every time we use the GPU
    tx = int(cuda.threadIdx.x)
    bx = int(cuda.blockIdx.x)
    dx = int(cuda.blockDim.x)

    ty = int(cuda.threadIdx.y)
    by = int(cuda.blockIdx.y)
    dy = int(cuda.blockDim.y)

    # Copy A(iy, ix) to s_matrix(ty, tx). Each block updates (dx-2)*(dy-2) elements
    # (iy, ix) is the global index
    iy = int((dy-2) * by + ty)
    ix = int((dx-2) * bx + tx)
    index = int(iy * (N+2)+ix)

    # Declare the shared memory on each block
    s_matrix = cuda.shared.array(shape=(NY, NX), dtype=float32)
    # Copy elements into shared memory
    if ix < N+2 and iy < N+2:
        s_matrix[ty, tx] = A[index]

    # Sync all threads in the block
    cuda.syncthreads()

    # update
    if iy < N+1 and ix < N+1:
        # Each block only updates interior elements, so we need to throw away the boundary element
        if ty > 0 and ty < dy and tx > 0 and tx < dx:
            # compute the number of neighbors
            n = int(s_matrix[ty+1, tx] + s_matrix[ty-1, tx]
                    + s_matrix[ty, tx+1] + s_matrix[ty, tx-1]
                    + s_matrix[ty+1, tx+1] + s_matrix[ty-1, tx-1]
                    + s_matrix[ty-1, tx+1] + s_matrix[ty+1, tx-1])

            # From shared memory to global memory
            oldState = int(s_matrix[ty, tx])
            newState = int((n==2 or n==3) if oldstate==1 else (n==3))
            A_new[index] = newState

# Function to convert from (i,j) cell index to linear storage index
def idx(N, i, j):
    n = int(i + (N+2)*j)
    return n

# Function to print the game board
def print_board(N, board):
    print("\n")
    for i in range(N+1)[1:]:
        for j in range(N+1)[1:]:
            print(board[idx(N, i, j)])
        print("\n")
    print("\n")

# Function to solve for the game board using the Game of Life rules
def solve(N):
    cuda.select_device(1);

    # Using C elements
    # Initializes integer random number generator
    libc = CDLL("libc.so.6")
    libc.srand(42)

    # Array is a memory holder
    hA_new = np.zeros((N+2)*(N+2), dtype=np.float)
    hA = np.zeros((N+2)+(N+2), dtype=np.float)

    for i in range(N+1)[1:]:
        for j in range(N+1)[1:]:
            # Sets the board state randomly to 1 or 0
            hA[idx(N, i, j)] = libc.rand()%2

    # Prints the initial board
    print("initial game board")
    print_board(N, hA)

    # Allocate an array on the DEVICE
    cA = cuda.to_device(hA)
    cA_new = cuda.to_device(cA_new)

    B = NX, NY, 1
    G = (N+NX-2-1)/(NX-2), (N+NY-2-1)/(NY-2), 1

    # Create events
    start = cuda.event()
    end = cuda.event()


    # Start recording
    start.record()

    # Iterations
    count = 0 # Step counter
    iostep = 1 # Output every iostep
    output = 1 # Save images if output=1
    maxsteps = 200 # Maximum number of steps

    # A false do-while loop
    while True:
        # Iterate from Iold to Inew
        GameOfLifeShared[G, B](N, cA, cA_new)

        # Iterate from Inew to Iold
        GameOfLifeShared[G, B](N, cA_new, cA)

        # From DEVICE to HOST
        hA = cA.copy_to_host()

        # Creates multiple images
        if output == 1 and count%iostep == 0:
            png = open("pyt%s.png" % count/iostep, 'w')
            write_gray_png(png, N+2, N+2, hA, 0, 1)
            png.close()

        # Update counter
        count = count + 1

        # WHen true, gets out of loop
        if hA_new is hA and count <= maxsteps:
            break

    end.record()

    elapsed = start.elapsed_time(end)
    elapsed = elapsed/1000. # Convert to seconds

    print("final game board:")
    print_board(N, hA)
    print("I_{1 1} = %d\n" % hA[idx(N,1,1)])
    print("I_{10 10} = %d\n" % hA[idx(N, 10, 10)])
    print("Took %d steps\n" % count)
    print("Time spent = %g sec\n" % elapsed)

def main(N):

    solve(N)

# So it can actually run
N = input("Input an N: ")
main(N)
