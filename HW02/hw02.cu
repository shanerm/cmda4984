#include <stdlib.h>
#include <stdio.h>
#include "cuda.h"

// write a CUDA kernel later
__global__ void fillKernel(int N, float val, float *c_x){

#if 0
  for(int n = 0; n<N; ++n){
    c_x[n] = val;
  }
#endif

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  // figure out n for this thread
  if(n<N) // n is legal index
    c_x[n] = val;
}

// Copying Data
__global__ void copyVectors(int N, float* a, float* c){
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N)
    c[n] = a[n];
}

// Adding Vectors
__global__ void addVectors(int N, float* a, float* d, float* c){
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N)
    c[n] = a[n] + d[n];
}

// Sine of Vectors
__global__ void sineVectors(int N, float* a, float* c){
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N)
    c[n] = sin(a[n]);
}

// HOST main function
int main(int argc, char **argv){

  // set the GPU
  cudaSetDevice(4);

  if(argc<2){ printf("usage: ./hw02 N\n"); exit(-1);  }

  // read command line argument
  int N = atoi(argv[1]);
  
  // allocate an array on the HOST
  float *h_x = (float*) malloc(N*sizeof(float));
  float val = 1.234567;
  float *h_y = (float*) malloc(N*sizeof(float));
  float *h_z = (float*) malloc(N*sizeof(float));
  float *h_a = (float*) malloc(N*sizeof(float));
  float *h_b = (float*) malloc(N*sizeof(float));

  // DEVICE VERSION:
  float *c_x;

  // allocate an array on the DEVICE
  cudaMalloc(&c_x, N*sizeof(float));

  // call kernel to set values in array
  int B = 256;
  int G = (N+B-1)/B;
  fillKernel <<< G , B >>> (N, val, c_x);

  // make a new vector
  float *c_y;

  // allocate an array on the DEVICE
  cudaMalloc(&c_y, N*sizeof(float));

  // set the values in array
  fillKernel <<< G , B >>> (N, 2*val, c_y);

  // copy the data
  float *c_z;

  // allocate an array on the DEVICE
  cudaMalloc(&c_z, N*sizeof(float));

  // Add the data from y and x
  float *c_a;

  // allocate an array on the DEVICE
  cudaMalloc(&c_a, N*sizeof(float));

  // Sine of x
  float *c_b;

  // allocate an array on the DEVICE
  cudaMalloc(&c_b, N*sizeof(float));

  // copy data from DEVICE to HOST
  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_y, c_y, N*sizeof(float), cudaMemcpyDeviceToHost);

  // set values of c_z
  copyVectors <<< G , B >>>(N, c_x, c_z);

  // set values of c_a
  addVectors <<< G , B >>> (N, c_x, c_y, c_a);

  // set values of c_b
  sineVectors <<< G , B >>> (N, c_x, c_b);

  // copy data from DEVICE to HOST
  cudaMemcpy(h_z, c_z, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_a, c_a, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_b, c_b, N*sizeof(float), cudaMemcpyDeviceToHost);

  // print out result
  for(int n=0; n<N; ++n){
    printf("h_x[%d] = %g\n", n, h_x[n]);
  }

  // print out new vector
  printf("New Vector:\n");
  for(int n=0; n<N; ++n){
    printf("h_y[%d] = %g\n", n, h_y[n]);
  }

  // print out copied vector
  printf("Copied Vector:\n");
  for(int n=0; n<N; ++n){
    printf("h_z[%d] = %g\n", n, h_z[n]);
  }

  // print out vector addition

  printf("Addition Vector of y_y and x_x:\n");
  for(int n=0; n<N; ++n){
    printf("h_a[%d] = %g\n", n, h_a[n]);
  }

  // print out axplby

  printf("Sine of x_x:\n");
  for(int n=0; n<N; ++n){
    printf("h_b[%d] = %g\n", n, h_b[n]);
  }
  
  printf("N=%d, G=%d, B=%d, G*B=%d\n", N, G, B, B*G);

  return 0;
}
