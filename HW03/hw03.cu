#include <stdlib.h>
#include <stdio.h>
#include "cuda.h"

// write a CUDA kernel later
__global__ void fillKernel(int N, float val, float *c_x){

#if 0
  for(int n = 0; n<N; ++n){
    c_x[n] = val;
  }
#endif

  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  // figure out n for this thread
  if(n<N) // n is legal index
    c_x[n] = val;
}

// Copying Data
__global__ void copyVectors(int N, float* a, float* c){
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N)
    c[n] = a[n];
}

// Adding Vectors
__global__ void addVectors(int N, float* a, float* d, float* c){
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N)
    c[n] = a[n] + d[n];
}

// Sine of Vectors
__global__ void sineVectors(int N, float* a, float* c){
  int t = threadIdx.x;
  int b = blockIdx.x;
  int B = blockDim.x;

  int n = t + b*B;

  if(n<N)
    c[n] = sin(a[n]);
}

// HOST main function
int main(int argc, char **argv){

  // set the GPU
  cudaSetDevice(4);

  if(argc<2){ printf("usage: ./hw02 N\n"); exit(-1);  }

  // read command line argument
  int N = atoi(argv[1]);
  
  // allocate an array on the HOST
  float *h_x = (float*) malloc(N*sizeof(float));
  float val = 1.234567;
  float *h_y = (float*) malloc(N*sizeof(float));
  float *h_z = (float*) malloc(N*sizeof(float));
  float *h_a = (float*) malloc(N*sizeof(float));
  float *h_b = (float*) malloc(N*sizeof(float));

  // DEVICE VERSION:
  float *c_x;

  // allocate an array on the DEVICE
  cudaMalloc(&c_x, N*sizeof(float));

  // call kernel to set values in array
  int B = 256;
  int G = (N+B-1)/B;
  fillKernel <<< G , B >>> (N, val, c_x);

  // make a new vector
  float *c_y;

  // allocate an array on the DEVICE
  cudaMalloc(&c_y, N*sizeof(float));

  // set the values in array
  fillKernel <<< G , B >>> (N, 2*val, c_y);

  // copy the data
  float *c_z;

  // allocate an array on the DEVICE
  cudaMalloc(&c_z, N*sizeof(float));

  // Add the data from y and x
  float *c_a;

  // allocate an array on the DEVICE
  cudaMalloc(&c_a, N*sizeof(float));

  // Sine of x
  float *c_b;

  // allocate an array on the DEVICE
  cudaMalloc(&c_b, N*sizeof(float));

  // timer
  cudaEvent_t start, end;

  cudaEventCreate(&start);
  cudaEventCreate(&end);

  // ability to record the time
  int cnt = 0;

  for(int p=0;p<N;++p){
    ++cnt;
  }

  float *recordEventDataSize = (float*) calloc(cnt, sizeof(float));
  double *recordEventTimes = (double*) calloc(cnt, sizeof(double));

  cnt = 0;

  // copy data from DEVICE to HOST
  cudaMemcpy(h_x, c_x, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_y, c_y, N*sizeof(float), cudaMemcpyDeviceToHost);

  cudaDeviceSynchronize();

  cudaEventRecord(start);
  // set values of c_z
  copyVectors <<< G , B >>>(N, c_x, c_z);
  cudaEventRecord(end);

  cudaDeviceSynchronize();

  // gets the time
  float elapsedEvent;
  cudaEventElapsedTime(&elapsedEvent, start, end);
  elapsedEvent /= 1000;
  
  recordEventDataSize[cnt] = N*sizeof(float)*2;
  recordEventTimes[cnt] = elapsedEvent;

  printf("%g, %g; %%%% B, time(s)\n", recordEventDataSize[cnt], recordEventTimes[cnt]);
  
  cudaDeviceSynchronize();

  cudaEventRecord(start);
  // set values of c_a
  addVectors <<< G , B >>> (N, c_x, c_y, c_a);
  cudaEventRecord(end);

  cudaDeviceSynchronize();

  // gets the time
  cnt = cnt+1;
  float aelapsedEvent;
  cudaEventElapsedTime(&aelapsedEvent, start, end);
  aelapsedEvent /= 1000;
  
  recordEventDataSize[cnt] = N*sizeof(float)*3;
  recordEventTimes[cnt] = aelapsedEvent;

  printf("%g, %g; %%%% B, time(s)\n", recordEventDataSize[cnt], recordEventTimes[cnt]);

  cudaDeviceSynchronize();

  cudaEventRecord(start);
  // set values of c_b
  sineVectors <<< G , B >>> (N, c_x, c_b);
  cudaEventRecord(end);

  cudaDeviceSynchronize();

  // gets the time
  cnt = cnt+1;
  float belapsedEvent;
  cudaEventElapsedTime(&belapsedEvent, start, end);
  belapsedEvent /= 1000;
  
  recordEventDataSize[cnt] = N*sizeof(float)*2;
  recordEventTimes[cnt] = belapsedEvent;

  printf("%g, %g; %%%% B, time(s)\n", recordEventDataSize[cnt], recordEventTimes[cnt]);

  // copy data from DEVICE to HOST
  cudaMemcpy(h_z, c_z, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_a, c_a, N*sizeof(float), cudaMemcpyDeviceToHost);
  cudaMemcpy(h_b, c_b, N*sizeof(float), cudaMemcpyDeviceToHost);
  
  printf("N=%d, G=%d, B=%d, G*B=%d\n", N, G, B, B*G);

  return 0;
}
